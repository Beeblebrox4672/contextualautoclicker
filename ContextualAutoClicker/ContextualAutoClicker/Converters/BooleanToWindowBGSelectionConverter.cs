﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContextualAutoClicker.Converters
{
    class BooleanToWindowBGSelectionConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo language)
        {
            return (value is bool && (bool)value) ? "#FF56BD00" : "#0056BD00";
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo language)
        {
            return value is string && value.ToString() == "#FF56BD00";
        }
    }
}
