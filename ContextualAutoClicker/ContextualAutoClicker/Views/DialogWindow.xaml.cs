﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ContextualAutoClicker.Commands;

namespace ContextualAutoClicker.Views
{
    /// <summary>
    /// Interaction logic for DialogWindow.xaml
    /// </summary>
    public partial class DialogWindow : Window
    {
        public DialogWindow()
        {
            InitializeComponent();
        }

        public DialogWindow(string Title, string Message) : this()
        {
            DialogTitle = Title;
            DialogMessage = Message;
        }

        public DialogWindow(string Title, string Message, string B1Text, Action B1Action) : this(Title, Message)
        {
            Button1Text = B1Text;
            Button1Action = B1Action;
        }

        public DialogWindow(string Title, string Message, string B1Text, Action B1Action, string B2Text, Action B2Action) : this(Title, Message, B1Text, B1Action)
        {
            Button2Text = B2Text;
            Button2Action = B2Action;
        }

        public DialogWindow(string Title, string Message, string B1Text, Action B1Action, string B2Text, Action B2Action, string B3Text, Action B3Action) : this(Title, Message, B1Text, B1Action, B2Text, B2Action)
        {
            Button3Text = B3Text;
            Button3Action = B3Action;
        }

        private void DragWindow(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        public string DialogTitle
        {
            get { return (string)GetValue(DialogTitleProperty); }
            set { SetValue(DialogTitleProperty, value); }
        }
        public static readonly DependencyProperty DialogTitleProperty =
            DependencyProperty.Register("DialogTitle", typeof(string),
            typeof(DialogWindow), new PropertyMetadata(""));

        public string DialogMessage
        {
            get { return (string)GetValue(DialogMessageProperty); }
            set { SetValue(DialogMessageProperty, value); }
        }
        public static readonly DependencyProperty DialogMessageProperty =
            DependencyProperty.Register("DialogMessage", typeof(string),
            typeof(DialogWindow), new PropertyMetadata(""));

        public string Button1Text
        {
            get { return (string)GetValue(Button1TextProperty); }
            set { SetValue(Button1TextProperty, value); }
        }
        public static readonly DependencyProperty Button1TextProperty =
            DependencyProperty.Register("Button1Text", typeof(string),
            typeof(DialogWindow), new PropertyMetadata(""));

        public string Button2Text
        {
            get { return (string)GetValue(Button2TextProperty); }
            set { SetValue(Button2TextProperty, value); }
        }
        public static readonly DependencyProperty Button2TextProperty =
            DependencyProperty.Register("Button2Text", typeof(string),
            typeof(DialogWindow), new PropertyMetadata(""));

        public string Button3Text
        {
            get { return (string)GetValue(Button3TextProperty); }
            set { SetValue(Button3TextProperty, value); }
        }
        public static readonly DependencyProperty Button3TextProperty =
            DependencyProperty.Register("Button3Text", typeof(string),
            typeof(DialogWindow), new PropertyMetadata(""));

        public Action Button1Action
        {
            get { return (Action)GetValue(Button1ActionProperty); }
            set { SetValue(Button1ActionProperty, value); }
        }
        public static readonly DependencyProperty Button1ActionProperty =
            DependencyProperty.Register("Button1Action", typeof(Action),
            typeof(DialogWindow), new PropertyMetadata(null));

        public Action Button2Action
        {
            get { return (Action)GetValue(Button2ActionProperty); }
            set { SetValue(Button2ActionProperty, value); }
        }
        public static readonly DependencyProperty Button2ActionProperty =
            DependencyProperty.Register("Button2Action", typeof(Action),
            typeof(DialogWindow), new PropertyMetadata(null));

        public Action Button3Action
        {
            get { return (Action)GetValue(Button3ActionProperty); }
            set { SetValue(Button3ActionProperty, value); }
        }
        public static readonly DependencyProperty Button3ActionProperty =
            DependencyProperty.Register("Button3Action", typeof(Action),
            typeof(DialogWindow), new PropertyMetadata(null));

        private ICommand _B1;
        public ICommand B1
        {
            get
            {
                if (_B1 == null)
                {
                    _B1 = new RelayCommand(B1Ex, null);
                }
                return _B1;
            }
        }
        private void B1Ex(object p)
        {
            this.Close();
            if (Button1Action != null)
            {
                Button1Action();
            }
        }

        private ICommand _B2;
        public ICommand B2
        {
            get
            {
                if (_B2 == null)
                {
                    _B2 = new RelayCommand(B2Ex, null);
                }
                return _B2;
            }
        }
        private void B2Ex(object p)
        {
            this.Close();
            if (Button2Action != null)
            {
                Button2Action();
            }
        }

        private ICommand _B3;
        public ICommand B3
        {
            get
            {
                if (_B3 == null)
                {
                    _B3 = new RelayCommand(B3Ex, null);
                }
                return _B3;
            }
        }
        private void B3Ex(object p)
        {
            this.Close();
            if (Button3Action != null)
            {
                Button3Action();
            }
        }
    }
}
