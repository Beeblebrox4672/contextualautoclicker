﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Enums
{
    enum ScreenshotPhase
    {
        NONE,
        IDLE,
        TOP_LEFT,
        BOTTOM_RIGHT,
        GRAB_IMAGE
    }
}
