﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContextualAutoClicker.UserControls
{
    /// <summary>
    /// Interaction logic for ClickActionSelector.xaml
    /// </summary>
    public partial class ClickActionSelector : UserControl
    {
        public ClickActionSelector()
        {
            InitializeComponent();
        }

        public ClickActionSelector(ICommand clickRelative, ICommand clickAbsolute, ICommand clickAbsoluteArea, ICommand moveRelative, ICommand moveAbsolute, ICommand activateWindow, ICommand pause, ICommand sendString, ICommand abort, ICommand close) : this()
        {
            this.ClickRelative = clickRelative;
            this.ClickAbsolute = clickAbsolute;
            this.ClickAbsoluteArea = clickAbsoluteArea;
            this.MoveRelative = moveRelative;
            this.MoveAbsolute = moveAbsolute;
            this.ActivateWindow = activateWindow;
            this.Pause = pause;
            this.SendString = sendString;
            this.Abort = abort;
            this.Close = close;
        }

        public ICommand ClickRelative
        {
            get { return (ICommand)GetValue(ClickRelativeProperty); }
            set { SetValue(ClickRelativeProperty, value); }
        }
        public static readonly DependencyProperty ClickRelativeProperty =
            DependencyProperty.Register("ClickRelative", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand ClickAbsolute
        {
            get { return (ICommand)GetValue(ClickAbsoluteProperty); }
            set { SetValue(ClickAbsoluteProperty, value); }
        }
        public static readonly DependencyProperty ClickAbsoluteProperty =
            DependencyProperty.Register("ClickAbsolute", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand ClickAbsoluteArea
        {
            get { return (ICommand)GetValue(ClickAbsoluteAreaProperty); }
            set { SetValue(ClickAbsoluteAreaProperty, value); }
        }
        public static readonly DependencyProperty ClickAbsoluteAreaProperty =
            DependencyProperty.Register("ClickAbsoluteArea", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand MoveAbsolute
        {
            get { return (ICommand)GetValue(MoveAbsoluteProperty); }
            set { SetValue(MoveAbsoluteProperty, value); }
        }
        public static readonly DependencyProperty MoveAbsoluteProperty =
            DependencyProperty.Register("MoveAbsolute", typeof(ICommand), typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand MoveRelative
        {
            get { return (ICommand)GetValue(MoveRelativeProperty); }
            set { SetValue(MoveRelativeProperty, value); }
        }
        public static readonly DependencyProperty MoveRelativeProperty =
            DependencyProperty.Register("MoveRelative", typeof(ICommand), typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand ActivateWindow
        {
            get { return (ICommand)GetValue(ActivateWindowProperty); }
            set { SetValue(ActivateWindowProperty, value); }
        }
        public static readonly DependencyProperty ActivateWindowProperty =
            DependencyProperty.Register("ActivateWindow", typeof(ICommand), typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand Pause
        {
            get { return (ICommand)GetValue(PauseProperty); }
            set { SetValue(PauseProperty, value); }
        }
        public static readonly DependencyProperty PauseProperty =
            DependencyProperty.Register("Pause", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand SendString
        {
            get { return (ICommand)GetValue(SendStringProperty); }
            set { SetValue(SendStringProperty, value); }
        }
        public static readonly DependencyProperty SendStringProperty =
            DependencyProperty.Register("SendString", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand Abort
        {
            get { return (ICommand)GetValue(AbortProperty); }
            set { SetValue(AbortProperty, value); }
        }
        public static readonly DependencyProperty AbortProperty =
            DependencyProperty.Register("Abort", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));

        public ICommand Close
        {
            get { return (ICommand)GetValue(CloseProperty); }
            set { SetValue(CloseProperty, value); }
        }
        public static readonly DependencyProperty CloseProperty =
            DependencyProperty.Register("Close", typeof(ICommand),
            typeof(ClickActionSelector), new PropertyMetadata(null));
    }
}
