﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ContextualAutoClicker.UserControls
{
    /// <summary>
    /// Interaction logic for SaveAs.xaml
    /// </summary>
    public partial class SaveAs : UserControl
    {
        public SaveAs()
        {
            InitializeComponent();
        }

        private void SaveAs_Loaded(object sender, RoutedEventArgs eventArgs)
        {
            this.EntryField.Focus();
        }
    }
}
