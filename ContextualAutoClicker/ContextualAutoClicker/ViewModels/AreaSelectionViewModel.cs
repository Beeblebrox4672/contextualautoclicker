﻿using ContextualAutoClicker.Commands;
using ContextualAutoClicker.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace ContextualAutoClicker.ViewModels
{
    class AreaSelectionViewModel : INotifyPropertychanged
    {
        public AreaSelectionViewModel(bool drawRectangle = true)
        {
            this.DrawRectangle = drawRectangle;
            if(drawRectangle)
            {
                Instructions = "Drag a rectangle to signify the area.";
            }
            else
            {
                Instructions = "Click and release at the point you wish to select.";
            }
        }

        private bool _DrawRectangle;
        public bool DrawRectangle
        {
            get
            {
                return _DrawRectangle;
            }
            set
            {
                _DrawRectangle = value;
                OnPropertyChanged("DrawRectangle");
            }
        }

        private string _Instructions;
        public string Instructions
        {
            get
            {
                return _Instructions;
            }
            set
            {
                _Instructions = value;
                OnPropertyChanged("Instructions");
            }
        }

        private Point _DownPoint;
        public Point DownPoint
        {
            get
            {
                return _DownPoint;
            }
            set
            {
                _DownPoint = value;
                OnPropertyChanged("DownPoint");
            }
        }

        private Point _UpPoint;
        public Point UpPoint
        {
            get
            {
                return _UpPoint;
            }
            set
            {
                _UpPoint = value;
                OnPropertyChanged("UpPoint");
            }
        }

        public Point TopLeft
        {
            get
            {
                int minX = Math.Min(UpPoint.X, DownPoint.X);
                int minY = Math.Min(UpPoint.Y, DownPoint.Y);
                return new Point(minX, minY);
            }
        }

        public int Width
        {
            get
            {
                return Math.Abs(UpPoint.X - DownPoint.X);
            }
        }

        public int Height
        {
            get
            {
                return Math.Abs(UpPoint.Y - DownPoint.Y);
            }
        }

        public Size Size
        {
            get
            {
                return new Size(Width, Height);
            }
        }

        private ICommand _OnMouseDown;
        public ICommand OnMouseDown
        {
            get
            {
                if (_OnMouseDown == null)
                {
                    _OnMouseDown = new RelayCommand(OnMouseDownEx, null);
                }
                return _OnMouseDown;
            }
        }
        private void OnMouseDownEx(object p)
        {
            DownPoint = WinAPI.GetCursorPosition();
        }

        public event EventHandler DataCaptured;

        private ICommand _OnMouseUp;
        public ICommand OnMouseUp
        {
            get
            {
                if (_OnMouseUp == null)
                {
                    _OnMouseUp = new RelayCommand(OnMouseUpEx, null);
                }
                return _OnMouseUp;
            }
        }
        private void OnMouseUpEx(object p)
        {
            UpPoint = WinAPI.GetCursorPosition();
            DataCaptured?.Invoke(this, new EventArgs());
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
