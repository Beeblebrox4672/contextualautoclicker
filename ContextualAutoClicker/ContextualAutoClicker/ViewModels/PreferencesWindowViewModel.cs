﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;
using ContextualAutoClicker.Commands;
using ContextualAutoClicker.Views;

namespace ContextualAutoClicker.ViewModels
{
    class PreferencesWindowViewModel : INotifyPropertyChanged
    {
        public PreferencesWindowViewModel()
        {
            LoadData();
        }

        #region BOOT
        private void LoadData()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 0:
                    BG0 = true;
                    break;
                case 1:
                    BG1 = true;
                    break;
                case 2:
                    BG2 = true;
                    break;
                case 3:
                    BG3 = true;
                    break;
                case 4:
                    BG4 = true;
                    break;
                case 5:
                    BG5 = true;
                    break;
                case 6:
                    BG6 = true;
                    break;
                case 7:
                    BG7 = true;
                    break;
                case 8:
                    BG8 = true;
                    break;
                case 9:
                    BG9 = true;
                    break;
                case 10:
                    BG10 = true;
                    break;
            }

            StopWarning = Properties.Settings.Default.PreventWarning;
        }
        #endregion

        #region PROPERTIES
        #region BACKGROUNDS
        private bool bg0 = false;
        public bool BG0
        {
            get
            {
                return bg0;
            }
            set
            {
                bg0 = value;
                OnPropertyChanged("BG0");
            }
        }

        private bool bg1 = false;
        public bool BG1
        {
            get
            {
                return bg1;
            }
            set
            {
                bg1 = value;
                OnPropertyChanged("BG1");
            }
        }

        private bool bg2 = false;
        public bool BG2
        {
            get
            {
                return bg2;
            }
            set
            {
                bg2 = value;
                OnPropertyChanged("BG2");
            }
        }

        private bool bg3 = false;
        public bool BG3
        {
            get
            {
                return bg3;
            }
            set
            {
                bg3 = value;
                OnPropertyChanged("BG3");
            }
        }

        private bool bg4 = false;
        public bool BG4
        {
            get
            {
                return bg4;
            }
            set
            {
                bg4 = value;
                OnPropertyChanged("BG4");
            }
        }

        private bool bg5 = false;
        public bool BG5
        {
            get
            {
                return bg5;
            }
            set
            {
                bg5 = value;
                OnPropertyChanged("BG5");
            }
        }

        private bool bg6 = false;
        public bool BG6
        {
            get
            {
                return bg6;
            }
            set
            {
                bg6 = value;
                OnPropertyChanged("BG6");
            }
        }

        private bool bg7 = false;
        public bool BG7
        {
            get
            {
                return bg7;
            }
            set
            {
                bg7 = value;
                OnPropertyChanged("BG7");
            }
        }

        private bool bg8 = false;
        public bool BG8
        {
            get
            {
                return bg8;
            }
            set
            {
                bg8 = value;
                OnPropertyChanged("BG8");
            }
        }

        private bool bg9 = false;
        public bool BG9
        {
            get
            {
                return bg9;
            }
            set
            {
                bg9 = value;
                OnPropertyChanged("BG9");
            }
        }

        private bool bg10 = false;
        public bool BG10
        {
            get
            {
                return bg10;
            }
            set
            {
                bg10 = value;
                OnPropertyChanged("BG10");
            }
        }
        #endregion
        private bool _StopWarning;
        public bool StopWarning
        {
            get
            {
                return _StopWarning;
            }
            set
            {
                _StopWarning = value;
                OnPropertyChanged("StopWarning");
            }
        }
        #region MISC
        private bool saved = false;
        public bool Saved
        {
            get
            {
                return saved;
            }
            set
            {
                saved = value;
                OnPropertyChanged("Saved");
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        #region BACKGROUND COMMANDS
        private ICommand selectBG;
        public ICommand SelectBG
        {
            get
            {
                if (selectBG == null)
                {
                    selectBG = new RelayCommand(SelectBGEx, null);
                }
                return selectBG;
            }
        }
        private void SelectBGEx(object p)
        {
            BG0 = false;
            BG1 = false;
            BG2 = false;
            BG3 = false;
            BG4 = false;
            BG5 = false;
            BG6 = false;
            BG7 = false;
            BG8 = false;
            BG9 = false;
            BG10 = false;

            switch (Convert.ToInt32(p))
            {
                case 0:
                    BG0 = true;
                    break;
                case 1:
                    BG1 = true;
                    break;
                case 2:
                    BG2 = true;
                    break;
                case 3:
                    BG3 = true;
                    break;
                case 4:
                    BG4 = true;
                    break;
                case 5:
                    BG5 = true;
                    break;
                case 6:
                    BG6 = true;
                    break;
                case 7:
                    BG7 = true;
                    break;
                case 8:
                    BG8 = true;
                    break;
                case 9:
                    BG9 = true;
                    break;
                case 10:
                    BG10 = true;
                    break;
            }
        }
        #endregion
        #region CLOSE WINDOW BUTTONS
        private ICommand savePrefs;
        public ICommand SavePrefs
        {
            get
            {
                if (savePrefs == null)
                {
                    savePrefs = new RelayCommand(SavePrefsEx, null);
                }
                return savePrefs;
            }
        }
        private void SavePrefsEx(object p)
        {
            Saved = true;
            var x = p as PreferencesWindow;
            x.Close();
        }

        private ICommand cancelPrefs;
        public ICommand CancelPrefs
        {
            get
            {
                if (cancelPrefs == null)
                {
                    cancelPrefs = new RelayCommand(CancelPrefsEx, null);
                }
                return cancelPrefs;
            }
        }
        private void CancelPrefsEx(object p)
        {
            var x = p as PreferencesWindow;
            x.Close();
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
