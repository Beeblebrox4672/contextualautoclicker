﻿using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ContextualAutoClicker.Commands;
using ContextualAutoClicker.Models;
using ContextualAutoClicker.Views;
using ContextualAutoClicker.UserControls;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics;
using System.Windows.Interop;
using GlobalHotKey;

namespace ContextualAutoClicker.ViewModels
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        private static readonly ILog Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private Window mainWindow;

        public MainWindowViewModel()
        {
            SettingsUpgrade();
            SetBGOpacity();
            CheckParameters();

            MainWindowContentControl = new MainContent();
            AddEventEx(null);
            IsDirty = false;

            RunCommandLineParameters();
        }

        public MainWindowViewModel(Window w) : this()
        {
            this.mainWindow = w;
            LogMessage("Program booted.");
            hotKey = hotKeyManager.Register(Key.F9, ModifierKeys.None);
            hotKeyManager.KeyPressed += HotKeyManagerPressed;
            SetMainButtonString(true);
        }

        #region BOOT
        private void SettingsUpgrade()
        {
            if (Properties.Settings.Default.NeedUpgrade)
            {
                Properties.Settings.Default.Upgrade();
                Properties.Settings.Default.NeedUpgrade = false;

                Properties.Settings.Default.Save();
            }
        }

        private void CheckParameters()
        {
            if (Application.Current.Properties["FileName"] != null || 
                Application.Current.Properties["Loops"] != null ||
                Application.Current.Properties["OpenMinimized"] != null)
            {
                //We opened up with parameters - Handle this here.

                OpenFileName = (String)Application.Current.Properties["FileName"];
                Loops = Convert.ToInt32(Application.Current.Properties["Loops"]);

                if(Application.Current.Properties["OpenMinimized"] != null)
                {
                    OpenMinimized = (Boolean)Application.Current.Properties["OpenMinimized"];
                }

                HasCLArgs = true;
            }
        }

        public void RunCommandLineParameters()
        {
            if (!HasCLArgs) return;

            if (OpenMinimized) mainWindow.WindowState = WindowState.Minimized;

            if (OpenFileName != null)
            {
                OpenFile();

                if (Loops > -1)
                {
                    RunEx(null);
                }
            }
        }
        #endregion

        #region HELPER FUNCTIONS
        private void OnMinimizeWindow(object sender, EventArgs e)
        {
            LogMessageThreaded("Minimizing Main Window", ELogLevel.ACTION);
            WindowState = WindowState.Minimized;
        }

        private void OnMaximizeWindow(object sender, EventArgs e)
        {
            LogMessageThreaded("Maximizing Main Window", ELogLevel.ACTION);
            WindowState = WindowState.Normal;
        }

        private void OnMarkDirty(object sender, EventArgs e)
        {
            IsDirty = true;
        }

        private void OnLogMessage(object sender, LogEventArgs e)
        {
            LogMessageThreaded(e.LM.Message, e.LM.Level);
        }

        private void OpenSaveAsInterface()
        {
            OverlayControl = new SaveAs();
        }

        private void SaveSettings(bool saveOver = false)
        {
            if(!Directory.Exists("Saves"))
            {
                Directory.CreateDirectory("Saves");
            }

            if(IsNewFile && !saveOver)
            {
                if(File.Exists("Saves\\" + SaveName + ".json"))
                {
                    OpenConfirmationOverlay("Are you sure you want to overwrite the file \"" + SaveName + ".json\"?", Save, DestroySaveAsConfirmation);
                    return;
                }
            }

            LogMessage("Saving Project");

            string JSON = JsonConvert.SerializeObject(Events, Formatting.Indented, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.Auto
            });

            File.WriteAllText("Saves\\" + SaveName + ".json", JSON);

            IsDirty = false;

            if (OverlayControl?.GetType() == typeof(SaveAs) || OverlayControl?.GetType() == typeof(Confirmation))
            {
                OverlayControl = null;
            }

            IsNewFile = false;

            if(mainWindow != null && requestExitAfterSave)
            {
                mainWindow.Close();
            }
        }

        private void OpenConfirmationOverlay(string message = "Are you sure?", ICommand OKCommand = null, ICommand cancelCommand = null)
        {
            OverlayControl = new Confirmation(message, OKCommand, cancelCommand);
        }

        private void OpenOpenFileInterface()
        {
            OverlayControl = new OpenFile();
            OpenFileName = string.Empty;
        }

        private void OpenFile()
        {
            if (!Directory.Exists("Saves"))
            {
                Directory.CreateDirectory("Saves");
            }

            if (!File.Exists("Saves\\" + OpenFileName))
            {
                dialogWindow = new DialogWindow("Missing file", "Could not find the file \"" + OpenFileName + "\".", "OK", () => dialogWindow.Close());
                if(mainWindow != null)
                {
                    dialogWindow.Owner = mainWindow;
                }
                dialogWindow.ShowDialog();
                OpenOpenFileInterface();
                return;
            }

            LogMessage("Opening file: " + OpenFileName);

            string JSON = File.ReadAllText("Saves\\" + OpenFileName);

            Events = JsonConvert.DeserializeObject<ObservableCollection<Event>>(JSON, new JsonSerializerSettings{
                TypeNameHandling = TypeNameHandling.Auto
            });

            foreach (Event e in Events)
            {
                e.MinimizeRequested -= OnMinimizeWindow;
                e.MaximizeRequested -= OnMaximizeWindow;
                e.MarkDirty -= OnMarkDirty;
                e.LogMessage -= OnLogMessage;

                e.MinimizeRequested += OnMinimizeWindow;
                e.MaximizeRequested += OnMaximizeWindow;
                e.MarkDirty += OnMarkDirty;
                e.LogMessage += OnLogMessage;

                e.SubscribeToAllClickEvents();
            }

            if(OverlayControl?.GetType() == typeof(OpenFile))
            {
                OverlayControl = null;
            }

            SaveName = OpenFileName.Remove(OpenFileName.Length - 5, 5);
            OpenFileName = string.Empty;
            IsNewFile = false;
            IsDirty = false;

            ClearLog();
        }
        
        private void RunLoop(object state)
        {
            requestStop = false;
            isRunning = true;

            ClearLog();
            LogMessageThreaded("Starting Loop...");
            TabIndex = 1;

            SetMainButtonString(false);

            if (!Properties.Settings.Default.PreventWarning)
            {
                Application.Current.Dispatcher.Invoke(new Action(() =>
                {
                    DialogWindow dw = new DialogWindow("Starting Up", "To end the running execution, hit F9 at any time." + Environment.NewLine + "Hide this prompt from Tools > Preferences.", "OK", null);
                    dw.Owner = mainWindow;
                    dw.ShowDialog();
                }));
            }
            
            if (Loops == 0)
            {
                Loops = int.MaxValue;
            }
            try
            {
                for (int i = 0; i < Loops; i++)
                {
                    CurrentLoop = i + 1;

                    Progress = (CurrentLoop / (float)Loops) * 100;

                    if (requestStop)
                    {
                        LogMessageThreaded("A request was made to abort the operation", ELogLevel.ERROR);
                        throw new GeneralError("A request was made to abort the operation");
                    }

                    LogMessageThreaded("Loop " + (i + 1) + " of " + Loops);

                    foreach (Event e in Events)
                    {
                        LogMessageThreaded("Starting group ID: " + e.ID);

                        bool progressed = false;
                        int attempts = 0;

                        while (!progressed)
                        {
                            if (requestStop)
                            {
                                LogMessageThreaded("Hotkey was used to stop operation", ELogLevel.ERROR);
                                throw new GeneralError("Hotkey was used to stop operation");
                            }

                            attempts++;

                            progressed = e.Run();
                            if (!progressed && attempts >= e.MaxAttempts && e.MaxAttempts > 0)
                            {
                                progressed = true;
                            }

                            GC.GetTotalMemory(true);

                            if (requestStop)
                            {
                                LogMessageThreaded("Hotkey was used to stop operation", ELogLevel.ERROR);
                                throw new GeneralError("Hotkey was used to stop operation");
                            }

                            Thread.Sleep(e.Delay);
                        }
                    }
                }

                LogMessageThreaded("Finished all loops!");
            }
            catch (MaxAttemptsException e)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => { OverlayControl = new Confirmation(e.Message, DestroyOverlay, DestroyOverlay); }));
            }
            catch (AbortEncounteredException e)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => { OverlayControl = new Confirmation(e.Message, DestroyOverlay, DestroyOverlay); }));
            }
            catch (GeneralError e)
            {
                Application.Current.Dispatcher.Invoke(new Action(() => { OverlayControl = new Confirmation(e.Message, DestroyOverlay, DestroyOverlay); }));
            }

            if(Loops == int.MaxValue)
            {
                Loops = 0;
            }

            if (Loops > 0 && Loops - CurrentLoop > 0)
            {
                Loops -= CurrentLoop - 1;
                LogMessageThreaded("Did not complete all loops. Deducting " + (CurrentLoop - 1) + " fromm the total as this is how many completed.");
            }

            isRunning = false;

            CurrentLoop = 0;

            SetMainButtonString(true);

/* If we started with command line arguments close the window now we've finished so the calling process can know when we've finished. */
            if (HasCLArgs)
            {
                LogMessageThreaded("Started with command line arguments so closing main window");
                Application.Current.Dispatcher.Invoke(new Action(() => { QuitEx(null); }));
            }
        }

        private void LogMessage(string message, ELogLevel level = ELogLevel.INFO)
        {
            LogMessages.Add(new LogMessage("[" + DateTime.Now.ToString("HH:mm:ss") + "]\t" + message, level));
            lock (logFileLock)
            {
                using (StreamWriter SW = File.AppendText(LOG_FILE_PATH))
                {
                    SW.WriteLine("[" + DateTime.Now.ToString("HH:mm:ss") + "]\t" + level.ToString() + "\t" + message);
                }
            }
        }

        private void LogMessageThreaded(string message, ELogLevel level = ELogLevel.INFO)
        {
            Application.Current.Dispatcher.Invoke(new Action(() => { LogMessage(message, level); }));
        }

        private void ClearLog()
        {
            Application.Current.Dispatcher.Invoke(new Action(() => { LogMessages.Clear(); }));
        }

        private void HotKeyManagerPressed(object sender, KeyPressedEventArgs e)
        {
            if (e.HotKey.Key == Key.F9)
            {
                requestStop = true;
            }
        }

        private void SetMainButtonString(bool ready)
        {
            if (ready)
            {
                MainButtonString = "GO!";
            }
            else
            {
                MainButtonString = "STOP! [ F9 ]";
            }
        }
        #endregion

        #region PROPERTIES
        private ObservableCollection<Event> _Events = new ObservableCollection<Event>();
        public ObservableCollection<Event> Events
        {
            get
            {
                return _Events;
            }
            set
            {
                if (_Events != value)
                {
                    _Events = value;
                    OnPropertyChanged("Events");
                }
            }
        }

        private ObservableCollection<LogMessage> _LogMessages = new ObservableCollection<LogMessage>();
        public ObservableCollection<LogMessage> LogMessages
        {
            get
            {
                return _LogMessages;
            }
            set
            {
                if(_LogMessages != value)
                {
                    _LogMessages = value;
                    OnPropertyChanged("LogMessages");
                }
            }
        }

        private WindowState _WindowState;
        public WindowState WindowState
        {
            get
            {
                return _WindowState;
            }
            set
            {
                if(_WindowState != value)
                {
                    _WindowState = value;
                    OnPropertyChanged("WindowState");
                }
            }
        }

        private string _SaveName = string.Empty;
        public string SaveName
        {
            get
            {
                return _SaveName;
            }
            set
            {
                if(value != _SaveName)
                {
                    _SaveName = value;
                    OnPropertyChanged("SaveName");
                    OnPropertyChanged("Title");
                }
            }
        }

        private bool _IsNewFile = true;
        public bool IsNewFile
        {
            get
            {
                return _IsNewFile;
            }
            set
            {
                if (_IsNewFile != value)
                {
                    _IsNewFile = value;
                    OnPropertyChanged("IsNewFile");
                    OnPropertyChanged("Title");
                }
            }
        }

        private bool _IsDirty = false;
        public bool IsDirty
        {
            get
            {
                return _IsDirty;
            }
            set
            {
                if(value != _IsDirty)
                {
                    _IsDirty = value;
                    OnPropertyChanged("IsDirty");
                    OnPropertyChanged("Title");
                }
            }
        }

        private int _Loops;
        public int Loops
        {
            get
            {
                return _Loops;
            }
            set
            {
                _Loops = value;
                OnPropertyChanged("Loops");
            }
        }

        private int _CurrentLoop;
        public int CurrentLoop
        {
            get
            {
                return _CurrentLoop;
            }
            set
            {
                _CurrentLoop = value;
                OnPropertyChanged("CurrentLoop");
            }
        }

        public string Title
        {
            get
            {
                string title = string.Empty;

                if (IsNewFile)
                {
                    title += "<Unsaved>";
                }
                else
                {
                    title += SaveName + ".json";
                }

                if(IsDirty)
                {
                    title += " *";
                }

                title += " :: Pingu's Contextual Auto Clicker";

                return title;
            }
        }

        private string _MainButtonString;
        public string MainButtonString
        {
            get
            {
                return _MainButtonString;
            }
            set
            {
                _MainButtonString = value;
                OnPropertyChanged("MainButtonString");
            }
        }

        private string _OpenFileName = string.Empty;
        public string OpenFileName
        {
            get
            {
                return _OpenFileName;
            }
            set
            {
                if(_OpenFileName != value)
                {
                    _OpenFileName = value;
                    OnPropertyChanged("OpenFileName");
                }
            }
        }

        private int _TabIndex;
        public int TabIndex
        {
            get
            {
                return _TabIndex;
            }
            set
            {
                if(value != _TabIndex)
                {
                    _TabIndex = value;
                    OnPropertyChanged("TabIndex");
                }
            }
        }

        private float _Progress;
        public float Progress
        {
            get
            {
                return _Progress;
            }
            set
            {
                _Progress = value;
                OnPropertyChanged("Progress");
            }
        }

        private bool _OpenMinimized = false;
        public bool OpenMinimized
        {
            get
            {
                return _OpenMinimized;
            }
            set
            {
                _OpenMinimized = value;
                OnPropertyChanged("OpenMinimized");
            }
        }

        private bool _HasCLArgs = false;
        public bool HasCLArgs
        {
            get
            {
                return _HasCLArgs;
            }
            set
            {
                _HasCLArgs = value;
                OnPropertyChanged("HasCLArgs");
            }
        }


        DialogWindow dialogWindow;

        private bool requestExitAfterSave = false;

        public static string LOG_FILE_PATH = @"Logs\LOG_" + Process.GetCurrentProcess().StartTime.ToString("yyMMdd-HHmmss") + ".log";
        static readonly object logFileLock = new object();

        HotKeyManager hotKeyManager = new HotKeyManager();
        HotKey hotKey;

        private bool isRunning = false;
        private bool requestStop = false;
        #region PANELS
        private UserControl _MainWindowContentControl;
        public UserControl MainWindowContentControl
        {
            get
            {
                return _MainWindowContentControl;
            }
            set
            {
                if (_MainWindowContentControl != value)
                {
                    _MainWindowContentControl = value;
                    OnPropertyChanged("MainWindowContentControl");
                }
            }
        }

        private UserControl _OverlayControl;
        public UserControl OverlayControl
        {
            get
            {
                return _OverlayControl;
            }
            set
            {
                if(_OverlayControl != value)
                {
                    _OverlayControl = value;
                    OnPropertyChanged("OverlayControl");
                }
            }
        }
        #endregion
        #region SETTINGS
        public string BGImage
        {
            get
            {
                if (Properties.Settings.Default.BackgroundImage == 0)
                {
                    return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".png";
                }
                return "Media/Backgrounds/Background" + Properties.Settings.Default.BackgroundImage + ".jpg";
            }
            set
            {
                OnPropertyChanged("BGImage");
            }
        }

        private double bgOpacity;
        public double BGOpacity
        {
            get
            {
                return bgOpacity;
            }
            set
            {
                bgOpacity = value;
                OnPropertyChanged("BGOpacity");
            }
        }
        private void SetBGOpacity()
        {
            switch (Properties.Settings.Default.BackgroundImage)
            {
                case 1:
                    BGOpacity = 0.1;
                    break;
                case 2:
                    BGOpacity = 0.1;
                    break;
                case 3:
                    BGOpacity = 0.3;
                    break;
                case 4:
                    BGOpacity = 0.6;
                    break;
                case 5:
                    BGOpacity = 0.2;
                    break;
                case 6:
                    BGOpacity = 0.6;
                    break;
                case 7:
                    BGOpacity = 0.2;
                    break;
                case 8:
                    BGOpacity = 0.2;
                    break;
                case 9:
                    BGOpacity = 0.1;
                    break;
                case 10:
                    BGOpacity = 0.5;
                    break;
                default:
                    BGOpacity = 0.5;
                    break;
            }
        }
        #endregion
        #region MISC
        private ProgramVersion _ProgramVersion = new ProgramVersion();
        public string ProgramVersion
        {
            get
            {
                return _ProgramVersion.Version;
            }
        }

        private string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                if (_Status != value)
                {
                    _Status = value;
                    OnPropertyChanged("Status");
                }
            }
        }
        #endregion
        #endregion

        #region COMMANDS
        private ICommand _Save;
        public ICommand Save
        {
            get
            {
                if(_Save == null)
                {
                    _Save = new RelayCommand(SaveEx, null);
                }
                return _Save;
            }
        }
        private void SaveEx(object p)
        {
            if(string.IsNullOrEmpty(SaveName))
            {
                OpenSaveAsInterface();
            }
            else
            {
                if(p == null)
                {
                    SaveSettings();
                }
                else
                {
                    SaveSettings((bool)p);
                }
            }
        }

        private ICommand _SaveAs;
        public ICommand SaveAs
        {
            get
            {
                if(_SaveAs == null)
                {
                    _SaveAs = new RelayCommand(SaveAsEx, null);
                }
                return _SaveAs;
            }
        }
        private void SaveAsEx(object p)
        {
            SaveName = string.Empty;
            OpenSaveAsInterface();
        }

        private ICommand _New;
        public ICommand New
        {
            get
            {
                if(_New == null)
                {
                    _New = new RelayCommand(NewEx, null);
                }
                return _New;
            }
        }
        private void NewEx(object p)
        {
            if(IsDirty && p == null)
            {
                OpenConfirmationOverlay("You have unsaved work in your current project - are you sure?", New, DestroyOverlay);
                return;
            }

            LogMessage("New file opened");

            ClearLog();

            Events = new ObservableCollection<Event>();
            Event.IDs = new List<int>();
            AddEventEx(null);
            IsNewFile = true;
            IsDirty = false;
            SaveName = string.Empty;

            if (OverlayControl?.GetType() == typeof(Confirmation))
            {
                OverlayControl = null;
            }
        }

        private ICommand _Open;
        public ICommand Open
        {
            get
            {
                if(_Open == null)
                {
                    _Open = new RelayCommand(OpenEx, null);
                }
                return _Open;
            }
        }
        private void OpenEx(object p)
        {
            if(IsDirty && p == null)
            {
                OpenConfirmationOverlay("You have unsaved work in your current project - are you sure?", Open, DestroyOverlay);
                return;
            }

            if(OpenFileName == string.Empty)
            {
                OpenOpenFileInterface();
                return;
            }

            OpenFile();
        }

        private ICommand _Quit;
        public ICommand Quit
        {
            get
            {
                if(_Quit == null)
                {
                    _Quit = new RelayCommand(QuitEx, null);
                }
                return _Quit;
            }
        }
        private void QuitEx(object p)
        {
            if(mainWindow != null)
            {
                mainWindow.Close();
            }
            else
            {
                dialogWindow = new DialogWindow("Error", "We were unable to close the window correctly.\nPlease close it using the window handles.", "OK", null);
                dialogWindow.ShowDialog();
            }
        }

        private ICommand _OpenFileBrowse;
        public ICommand OpenFileBrowse
        {
            get
            {
                if(_OpenFileBrowse == null)
                {
                    _OpenFileBrowse = new RelayCommand(OpenFileBrowseEx, null);
                }
                return _OpenFileBrowse;
            }
        }
        private void OpenFileBrowseEx(object p)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "json files (*.json)|*.json;";
            ofd.InitialDirectory = Path.Combine(Directory.GetCurrentDirectory(), "Saves");
            ofd.Multiselect = false;
            ofd.ShowDialog();
            OpenFileName = ofd.SafeFileName;
        }

        private ICommand _AddEvent;
        public ICommand AddEvent
        {
            get
            {
                if (_AddEvent == null)
                {
                    _AddEvent = new RelayCommand(AddEventEx, null);
                }
                return _AddEvent;
            }
        }
        private void AddEventEx(object p)
        {
            LogMessage("Adding new group");
            Event e = new Event();
            e.MinimizeRequested += OnMinimizeWindow;
            e.MaximizeRequested += OnMaximizeWindow;
            e.MarkDirty += OnMarkDirty;
            e.LogMessage += OnLogMessage;
            e.AddClickEx(null);
            Events.Add(e);
            IsDirty = true;
        }

        private ICommand _RemoveEvent;
        public ICommand RemoveEvent
        {
            get
            {
                if (_RemoveEvent == null)
                {
                    _RemoveEvent = new RelayCommand(RemoveEventEx, null);
                }
                return _RemoveEvent;
            }
        }
        private void RemoveEventEx(object p)
        {
            int eventId = -1;
            if(p != null)
                int.TryParse(p.ToString(), out eventId);

            LogMessage("Removing group ID: " + eventId);

            Event eventToRemove = Events.SingleOrDefault(e => e.ID == eventId);
            eventToRemove.Kill();
            Events.Remove(eventToRemove);
            IsDirty = true;
        }

        private ICommand _CloseSaveAs;
        public ICommand CloseSaveAs
        {
            get
            {
                if (_CloseSaveAs == null)
                {
                    _CloseSaveAs = new RelayCommand(CloseSaveAsEx, null);
                }
                return _CloseSaveAs;
            }
        }
        private void CloseSaveAsEx(object p)
        {
            DestroyOverlayEx(p);
            SaveName = string.Empty;
            requestExitAfterSave = false;
        }

        private ICommand _DestroySaveAsConfirmation;
        public ICommand DestroySaveAsConfirmation
        {
            get
            {
                if(_DestroySaveAsConfirmation == null)
                {
                    _DestroySaveAsConfirmation = new RelayCommand(DestroySaveAsConfirmationEx, null);
                }
                return _DestroySaveAsConfirmation;
            }
        }
        private void DestroySaveAsConfirmationEx(object p)
        {
            DestroyOverlayEx(p);
            SaveName = string.Empty;
            requestExitAfterSave = false;
        }

        private ICommand _DestroyOverlay;
        public ICommand DestroyOverlay
        {
            get
            {
                if(_DestroyOverlay == null)
                {
                    _DestroyOverlay = new RelayCommand(DestroyOverlayEx, null);
                }
                return _DestroyOverlay;
            }
        }
        private void DestroyOverlayEx(object p)
        {
            OverlayControl = null;
            requestExitAfterSave = false;
        }

        private ICommand _Run;
        public ICommand Run
        {
            get
            {
                if (_Run == null)
                {
                    _Run = new RelayCommand(RunEx, null);
                }
                return _Run;
            }
        }
        private void RunEx(object p)
        {
            if(isRunning)
            {
                requestStop = true;
            }
            else
            {
                ThreadPool.QueueUserWorkItem(new WaitCallback(RunLoop));
            }
        }

        private ICommand _OpenHelp;
        public ICommand OpenHelp
        {
            get
            {
                if (_OpenHelp == null)
                {
                    _OpenHelp = new RelayCommand(OpenHelpEx, null);
                }
                return _OpenHelp;
            }
        }
        private void OpenHelpEx(object p)
        {
            string helpText = "This tool has three levels of depth. At the top level, is a list of 0 or more groups. Each group has a list of 0 or more scenarios, and each scenario has a list of 0 or more actions.\n\n";
            helpText += "After setup, you will set the number of loops to run at the top then hit go. The first group will then begin telling each of its scenarios to check if they should trigger, and if so, run their actions.\n\n";
            helpText += "A group can have a maximum number of attempts, after which it will pass control over to the next group in the list. It also has a configurable delay, between 50 and 1,000 ms. If a scenario triggers, and that scenario is set to \"next group on trigger\", then the group will also pass control onto the next group.\n\n";
            helpText += "Scenarios can trigger in 2 ways. If Always Trigger is set, then they will always trigger whenever the group they are in polls them. Otherwise, they will search for the an image, which you can set by taking a screenshot. It will search within the defined search area.\n\n";
            helpText += "If a scenario triggers, then it will perform each of the actions in its list, as defined. If it is set to \"next group on trigger\", then the next group will be polled next.\n\n";
            helpText += "When the final group attempts to move to the next group, it will return to the first group in the list, and the loop counter will increment by 1. Operation continues until the desired number of loops have been met.\n\n";
            helpText += "At any point during operation, hit F9 and it will cancle running ASAP.";
            OverlayControl = new Confirmation(helpText, DestroyOverlay, DestroyOverlay);
        }

        private ICommand _EmailSupport;
        public ICommand EmailSupport
        {
            get
            {
                if (_EmailSupport == null)
                {
                    _EmailSupport = new RelayCommand(EmailSupportEx, null);
                }
                return _EmailSupport;
            }
        }
        private void EmailSupportEx(object p)
        {
            DialogWindow dw = new DialogWindow("Contact Support", "Support is not guaranteed, but you can email \"pingu@pinguapps.com\" for a support request.", "OK", () => { });
            dw.Owner = mainWindow;
            dw.ShowDialog();
        }

        private ICommand _OpenAbout;
        public ICommand OpenAbout
        {
            get
            {
                if (_OpenAbout == null)
                {
                    _OpenAbout = new RelayCommand(OpenAboutEx, null);
                }
                return _OpenAbout;
            }
        }
        private void OpenAboutEx(object p)
        {
            DialogWindow dw = new DialogWindow("About", "Created by Pingu of PinguApps." + Environment.NewLine + "Contains helpful edits from Beeblebrox.", "OK", () => { });
            dw.Owner = mainWindow;
            dw.ShowDialog();
        }

        private ICommand _Donate;
        public ICommand Donate
        {
            get
            {
                if (_Donate == null)
                {
                    _Donate = new RelayCommand(DonateEx, null);
                }
                return _Donate;
            }
        }
        private void DonateEx(object p)
        {
            Process.Start("https://paypal.me/PinguApps");
        }

        private ICommand _OpenSaveFile;
        public ICommand OpenSaveFile
        {
            get
            {
                if (_OpenSaveFile == null)
                {
                    _OpenSaveFile = new RelayCommand(OpenSaveFileEx, null);
                }
                return _OpenSaveFile;
            }
        }
        private void OpenSaveFileEx(object p)
        {
            if (!Directory.Exists("Saves"))
            {
                Directory.CreateDirectory("Saves");
            }
            
            Process.Start(Directory.GetCurrentDirectory() + "\\Saves");
        }

        #region MISC
        private ICommand _OpenPreferences;
        public ICommand OpenPreferences
        {
            get
            {
                if (_OpenPreferences == null)
                {
                    _OpenPreferences = new RelayCommand(OpenPreferencesEx, null);
                }
                return _OpenPreferences;
            }
        }
        private void OpenPreferencesEx(object p)
        {
            LogMessage("Opening preferences window");
            PreferencesWindow PrefWindow = new PreferencesWindow();
            PreferencesWindowViewModel PrefWindowViewModel = new PreferencesWindowViewModel();
            PrefWindow.DataContext = PrefWindowViewModel;
            PrefWindow.Owner = p as MainWindow;
            PrefWindow.ShowDialog();

            if (PrefWindowViewModel.Saved)
            {
                if (PrefWindowViewModel.BG0)
                {
                    Properties.Settings.Default.BackgroundImage = 0;
                }
                if (PrefWindowViewModel.BG1)
                {
                    Properties.Settings.Default.BackgroundImage = 1;
                }
                if (PrefWindowViewModel.BG2)
                {
                    Properties.Settings.Default.BackgroundImage = 2;
                }
                if (PrefWindowViewModel.BG3)
                {
                    Properties.Settings.Default.BackgroundImage = 3;
                }
                if (PrefWindowViewModel.BG4)
                {
                    Properties.Settings.Default.BackgroundImage = 4;
                }
                if (PrefWindowViewModel.BG5)
                {
                    Properties.Settings.Default.BackgroundImage = 5;
                }
                if (PrefWindowViewModel.BG6)
                {
                    Properties.Settings.Default.BackgroundImage = 6;
                }
                if (PrefWindowViewModel.BG7)
                {
                    Properties.Settings.Default.BackgroundImage = 7;
                }
                if (PrefWindowViewModel.BG8)
                {
                    Properties.Settings.Default.BackgroundImage = 8;
                }
                if (PrefWindowViewModel.BG9)
                {
                    Properties.Settings.Default.BackgroundImage = 9;
                }
                if (PrefWindowViewModel.BG10)
                {
                    Properties.Settings.Default.BackgroundImage = 10;
                }
                Properties.Settings.Default.PreventWarning = PrefWindowViewModel.StopWarning;

                Properties.Settings.Default.Save();
                OnPropertyChanged("BGImage");
                SetBGOpacity();
            }
        }

        public void WindowClosing(CancelEventArgs e)
        {
            //Mark this as true if you wish to potentiolly cancel the window closing.

            if (IsDirty)
            {

                dialogWindow = new DialogWindow("Unsaved work", "You have unsaved changes to the current project\nDo you want to save before?",
                                                "Save", () => { SaveEx(null); e.Cancel = true; requestExitAfterSave = true; },
                                                "Don't Save", () => { },
                                                "Cancel", () => e.Cancel = true);
                if (mainWindow != null)
                {
                    dialogWindow.Owner = mainWindow;
                }
                dialogWindow.ShowDialog();
            }
        }

        private ICommand _ThemedDialog;
        public ICommand ThemedDialog
        {
            get
            {
                if (_ThemedDialog == null)
                {
                    _ThemedDialog = new RelayCommand(ThemedDialogEx, null);
                }
                return _ThemedDialog;
            }
        }
        private void ThemedDialogEx(object p)
        {
            DialogWindow DialogWindow = new DialogWindow("Title", "Message", "PRESS ME", null);
            DialogWindow.Owner = p as MainWindow;
            DialogWindow.ShowDialog();
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
