﻿using ContextualAutoClicker.Commands;
using ContextualAutoClicker.ViewModels;
using ContextualAutoClicker.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Threading;

namespace ContextualAutoClicker.Models
{
    class ClickAbsoluteAreaAction : BaseAction
    {
        public ClickAbsoluteAreaAction() : base()
        {

        }

        public ClickAbsoluteAreaAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle location)
        {
            Log("Clicking " + NumberOfClicks + " times in area top left: " + TopLeft + ", Width: " + Width + ", Height: " + Height, ELogLevel.ACTION);

            Random r = new Random();

            for (int i = 0; i < NumberOfClicks; i++)
            {
                int x = TopLeft.X + r.Next(0, Width);
                int y = TopLeft.Y + r.Next(0, Height);

                Point Location = new Point(x, y);

                WinAPI.MoveAndClick(Location);

                Thread.Sleep(50);
            }
        }

        #region METHODS
        private void FinishLocationCapture(object sender, EventArgs e)
        {
            areaSelectWindow?.Close();
            RequestMaximize(e);

            TopLeft = viewModel.TopLeft;
            Width = viewModel.Width;
            Height = viewModel.Height;
        }
        #endregion

        #region PROPERTIES
        private Point _TopLeft;
        public Point TopLeft
        {
            get
            {
                return _TopLeft;
            }
            set
            {
                _TopLeft = value;
                OnPropertyChanged("TopLeft");
                RequestMarkDirty(new EventArgs());
            }
        }
        private int _Width;
        public int  Width
        {
            get
            {
                return _Width;
            }
            set
            {
                _Width = value;
                OnPropertyChanged("Width");
                RequestMarkDirty(new EventArgs());
            }
        }
        private int _Height;
        public int Height
        {
            get
            {
                return _Height;
            }
            set
            {
                _Height = value;
                OnPropertyChanged("Height");
                RequestMarkDirty(new EventArgs());
            }
        }
        private int _NumberOfClicks = 1;
        public int NumberOfClicks
        {
            get
            {
                return _NumberOfClicks;
            }
            set
            {
                _NumberOfClicks = value;
                OnPropertyChanged("NumberOfClicks");
                RequestMarkDirty(new EventArgs());
            }
        }
        #endregion

        #region VARIABLES
        private AreaSelectionWindow areaSelectWindow;
        private AreaSelectionViewModel viewModel;
        #endregion

        #region ICOMMANDS
        private ICommand _SetLocation;
        public ICommand SetLocation
        {
            get
            {
                if (_SetLocation == null)
                {
                    _SetLocation = new RelayCommand(SetLocationEx, null);
                }
                return _SetLocation;
            }
        }
        private void SetLocationEx(object p)
        {
            RequestMinimize(new EventArgs());

            areaSelectWindow = new AreaSelectionWindow(true);

            viewModel = new AreaSelectionViewModel(true);

            viewModel.DataCaptured += FinishLocationCapture;

            areaSelectWindow.DataContext = viewModel;

            areaSelectWindow.Show();
        }
        #endregion
    }
}
