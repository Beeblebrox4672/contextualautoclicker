﻿using ContextualAutoClicker.Commands;
using ContextualAutoClicker.ViewModels;
using ContextualAutoClicker.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContextualAutoClicker.Models
{
    class MoveAbsoluteAction : BaseAction
    {
        public MoveAbsoluteAction() : base()
        {

        }

        public MoveAbsoluteAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle location)
        {
            Log("Move absolute location: " + Location, ELogLevel.ACTION);
            WinAPI.MoveMouse(Location);
        }

        #region METHODS
        private void FinishLocationCapture(object sender, EventArgs e)
        {
            areaSelectWindow?.Close();
            RequestMaximize(e);

            Location = viewModel.UpPoint;
        }
        #endregion

        #region PROPERTIES
        private Point _Location;
        public Point Location
        {
            get
            {
                return _Location;
            }
            set
            {
                _Location = value;
                OnPropertyChanged("Location");
                RequestMarkDirty(new EventArgs());
            }
        }
        #endregion

        #region VARIABLES
        private AreaSelectionWindow areaSelectWindow;
        private AreaSelectionViewModel viewModel;
        #endregion

        #region ICOMMANDS
        private ICommand _SetLocation;
        public ICommand SetLocation
        {
            get
            {
                if (_SetLocation == null)
                {
                    _SetLocation = new RelayCommand(SetLocationEx, null);
                }
                return _SetLocation;
            }
        }
        private void SetLocationEx(object p)
        {
            RequestMinimize(new EventArgs());

            areaSelectWindow = new AreaSelectionWindow(false);

            viewModel = new AreaSelectionViewModel(false);

            viewModel.DataCaptured += FinishLocationCapture;

            areaSelectWindow.DataContext = viewModel;

            areaSelectWindow.Show();
        }
        #endregion
    }
}
