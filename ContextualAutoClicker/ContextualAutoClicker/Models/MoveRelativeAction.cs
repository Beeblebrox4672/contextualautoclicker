﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class MoveRelativeAction : BaseAction
    {
        public MoveRelativeAction() : base()
        {

        }

        public MoveRelativeAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle location)
        {
            if (location == null || (location.Width <= 0 && location.Height <= 0))
            {
                Log("Location was null for a relative click action", ELogLevel.ERROR);
                throw new GeneralError("Location was null for a Relative Click Action.");
            }

            System.Drawing.Point p = new Point(location.X + (location.Width / 2), location.Y + (location.Height / 2));

            Log("Move mouse relative location: " + location, ELogLevel.ACTION);
            WinAPI.MoveMouse(p);
        }

        #region PROPERTIES

        #endregion
    }
}
