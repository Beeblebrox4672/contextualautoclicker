﻿using ContextualAutoClicker.Commands;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Input;

namespace ContextualAutoClicker.Models
{
    class Event : INotifyPropertyChanged
    {
        #region CTORs
        public Event()
        {
            AssignNewID();
        }
        #endregion

        #region METHODS
        private void AssignNewID()
        {
            ID = Enumerable.Range(0, int.MaxValue).Except(IDs).FirstOrDefault();

            IDs.Add(ID);
        }

        private int GetNextClickID()
        {
            return Enumerable.Range(0, int.MaxValue).Except(clickIDs).FirstOrDefault();
        }

        public void Kill()
        {
            IDs.Remove(ID);
        }

        public void SubscribeToAllClickEvents()
        {
            foreach (Click click in Clicks)
            {
                click.MinimizeRequested -= RequestMinimize;
                click.MaximizeRequested -= RequestMaximize;
                click.MarkDirty -= RequestMarkDirty;
                click.LogMessage -= Log;

                click.MinimizeRequested += RequestMinimize;
                click.MaximizeRequested += RequestMaximize;
                click.MarkDirty += RequestMarkDirty;
                click.LogMessage += Log;

                click.SubscribeToAllActionEvents();
            }
        }

        public bool Run()
        {
            bool progress = false;

            foreach (Click c in Clicks)
            {
                bool progressThisClick = c.Run() && c.Proceed;
                if (progressThisClick)
                    progress = progressThisClick;
            }

            return progress;
        }
        #endregion

        #region EVENTS
        public event EventHandler MinimizeRequested;
        public event EventHandler MaximizeRequested;
        public event EventHandler MarkDirty;
        public event EventHandler<LogEventArgs> LogMessage;

        private void RequestMinimize(object sender, EventArgs e)
        {
            MinimizeRequested?.Invoke(this, e);
        }

        private void RequestMaximize(object sender, EventArgs e)
        {
            MaximizeRequested?.Invoke(this, e);
        }

        private void RequestMarkDirty(object sender, EventArgs e)
        {
            MarkDirty?.Invoke(sender, e);
        }

        private void Log(object sender, LogEventArgs e)
        {
            LogMessage?.Invoke(sender, e);
        }

        private void Log(LogEventArgs e)
        {
            Log(this, e);
        }

        private void Log(string message, ELogLevel level = ELogLevel.INFO)
        {
            Log(new LogEventArgs(message, level));
        }
        #endregion

        #region PROPERTIES
        private int _ID;
        [JsonProperty]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (_ID != value)
                {
                    _ID = value;
                    OnPropertyChanged("ID");
                }
            }
        }

        [JsonProperty]
        public static List<int> IDs = new List<int>();
        [JsonProperty]
        private List<int> clickIDs = new List<int>();

        private ObservableCollection<Click> _Clicks = new ObservableCollection<Click>();
        [JsonProperty]
        public ObservableCollection<Click> Clicks
        {
            get
            {
                return _Clicks;
            }
            set
            {
                if (_Clicks != value)
                {
                    _Clicks = value;
                    OnPropertyChanged("_Clicks");
                }
            }
        }

        private int _MaxAttempts = 0;
        [JsonProperty]
        public int MaxAttempts
        {
            get
            {
                return _MaxAttempts;
            }
            set
            {
                if(value != _MaxAttempts)
                {
                    _MaxAttempts = value;
                    OnPropertyChanged("MaxAttempts");
                    RequestMarkDirty(this, new EventArgs());
                }
            }
        }

        private int _Delay = 50;
        [JsonProperty]
        public int Delay
        {
            get
            {
                return _Delay;
            }
            set
            {
                if (value < 50)
                    value = 50;
                if (value > 1000)
                    value = 1000;
                if (_Delay != value)
                {
                    _Delay = value;
                    OnPropertyChanged("Delay");
                    RequestMarkDirty(this, new EventArgs());
                }
            }
        }
        #endregion

        #region ICOMMANDS
        private ICommand _AddClick;
        [JsonIgnore]
        public ICommand AddClick
        {
            get
            {
                if (_AddClick == null)
                {
                    _AddClick = new RelayCommand(AddClickEx, null);
                }
                return _AddClick;
            }
        }
        public void AddClickEx(object p)
        {
            int newClickID = GetNextClickID();
            Log("Adding scenario ID: " + newClickID);
            Click click = new Click(newClickID);
            click.MinimizeRequested += RequestMinimize;
            click.MaximizeRequested += RequestMaximize;
            click.MarkDirty += RequestMarkDirty;
            click.LogMessage += Log;
            Clicks.Add(click);
            clickIDs.Add(newClickID);
            RequestMarkDirty(this, new EventArgs());
        }

        private ICommand _RemoveClick;
        [JsonIgnore]
        public ICommand RemoveClick
        {
            get
            {
                if (_RemoveClick == null)
                {
                    _RemoveClick = new RelayCommand(RemoveClickEx, null);
                }
                return _RemoveClick;
            }
        }
        private void RemoveClickEx(object p)
        {
            int clickID = -1;
            if (p != null)
                int.TryParse(p.ToString(), out clickID);

            Log("Removing scenarion ID: " + clickID);

            Click clickToRemove = Clicks.SingleOrDefault(e => e.ID == clickID);
            clickIDs.Remove(clickID);
            Clicks.Remove(clickToRemove);
            RequestMarkDirty(this, new EventArgs());
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
