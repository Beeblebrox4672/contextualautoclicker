﻿using ContextualAutoClicker.Commands;
using ContextualAutoClicker.Converters;
using ContextualAutoClicker.Enums;
using ContextualAutoClicker.UserControls;
using ContextualAutoClicker.ViewModels;
using ContextualAutoClicker.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace ContextualAutoClicker.Models
{
    class Click : INotifyPropertyChanged
    {
        #region CTORs
        public Click()
        {
            ScreenshotButtonText = "Take Screenshot...";
        }

        public Click(int ID) : this()
        {
            this.ID = ID;
        }
        #endregion

        #region METHODS
        private void FinishScreenshot(object sender, EventArgs e)
        {
            areaSelectWindow?.Close();
            RequestMaximize(e);
            ScreenshotButtonText = "3...";
            timer = new Timer(TimerTick, null, 1000, 1000);
            Log("Started countdown for screenshot", ELogLevel.INFO);
        }

        private void TimerTick(object state)
        {
            switch (ScreenshotButtonText)
            {
                case "3...":
                    ScreenshotButtonText = "2...";
                    break;
                case "2...":
                    ScreenshotButtonText = "1...";
                    break;
                case "1...":
                    Log("Screenshotting the selected image");
                    Bitmap fs = GetFullScreenShot();
                    Image = fs.Clone(new Rectangle(viewModel.TopLeft, viewModel.Size), fs.PixelFormat);
                    ScreenshotButtonText = "Take Screenshot...";
                    timer.Dispose();
                    RequestMarkDirty(new EventArgs());
                    break;
                default:
                    Log("TimerTick is ticking and its switch is defaulting.", ELogLevel.ERROR);
                    break;
            }
        }

        internal void SubscribeToAllActionEvents()
        {
            foreach (BaseAction action in Actions)
            {
                action.LogMessage -= Log;
                action.MarkDirty -= RequestMarkDirty;
                action.MaximizeRequested -= RequestMaximize;
                action.MinimizeRequested -= RequestMinimize;

                action.LogMessage += Log;
                action.MarkDirty += RequestMarkDirty;
                action.MaximizeRequested += RequestMaximize;
                action.MinimizeRequested += RequestMinimize;
            }
        }

        private Bitmap GetFullScreenShot()
        {
            var bmpScreenshot = new Bitmap((int)SystemParameters.VirtualScreenWidth, (int)SystemParameters.VirtualScreenHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // Create a graphics object from the bitmap.
            var gfxScreenshot = Graphics.FromImage(bmpScreenshot);

            // Take the screenshot from the upper left corner to the right bottom corner.
            gfxScreenshot.CopyFromScreen(0, 0, 0, 0, new System.Drawing.Size((int)SystemParameters.VirtualScreenWidth, (int)SystemParameters.VirtualScreenHeight), CopyPixelOperation.SourceCopy);

            return bmpScreenshot;
        }

        private void FinishCaptureArea(object sender, EventArgs e)
        {
            areaSelectWindow?.Close();
            RequestMaximize(e);

            SearchAreaTopLeft = viewModel.TopLeft;
            SearchAreaSize = viewModel.Size;

            RequestMarkDirty(e);
        }

        private int GetNextActionID()
        {
            return Enumerable.Range(0, int.MaxValue).Except(ActionIDs).FirstOrDefault();
        }

        public bool Run()
        {
            bool triggered = false;

            Rectangle location = new Rectangle(0,0,0,0);

            if (AlwaysTrigger)
            {
                triggered = true;
                Log("Scenario ID: " + ID + " will trigger - Always trigger checked", ELogLevel.CHECK);
            }
            else
            {
                if (Image != null)
                {
                    if (SearchAreaSize.Width >= Image.Width && SearchAreaSize.Height >= Image.Height)
                    {
                        Log("Searching for image in scenario ID: " + ID, ELogLevel.CHECK);
                        Bitmap searchArea = WinAPI.CropImage(WinAPI.GetFullScreenShot(), SearchAreaTopLeft, SearchAreaSize);
                        
                        location = WinAPI.SearchBitmapLocation(Image, searchArea, 0.1);
                        
                        if (location.Width > 0 || location.Height > 0)
                        {
                            triggered = true;
                            Log("Scenario ID: " + ID + " will trigger - Image found", ELogLevel.CHECK);
                        }
                        else
                        {
                            Log("Scenario ID: " + ID + " will not trigger - Image not found");
                        }
                    }
                }
            }

            if (triggered)
            {
                // We run each action here.

                foreach (BaseAction a in Actions)
                {
                    if (a.GetType() == typeof(AbortAction))
                        throw new AbortEncounteredException("An abort action was encountered.");
                    a.Run(new Rectangle(SearchAreaTopLeft.X + location.X, SearchAreaTopLeft.Y + location.Y, location.Width, location.Height));
                    Thread.Sleep(25);
                }
            }

            return triggered;
        }
        #endregion

        #region EVENTS
        public event EventHandler MinimizeRequested;
        public event EventHandler MaximizeRequested;
        public event EventHandler MarkDirty;
        public event EventHandler<LogEventArgs> LogMessage;

        protected virtual void RequestMinimize(EventArgs e)
        {
            RequestMinimize(this, e);
        }

        protected virtual void RequestMaximize(EventArgs e)
        {
            RequestMaximize(this, e);
        }

        protected virtual void RequestMarkDirty(EventArgs e)
        {
            RequestMarkDirty(this, e);
        }

        protected virtual void RequestMarkDirty(object sender, EventArgs e)
        {
            MarkDirty?.Invoke(sender, e);
        }

        protected virtual void RequestMinimize(object sender, EventArgs e)
        {
            MinimizeRequested?.Invoke(sender, e);
        }

        protected virtual void RequestMaximize(object sender, EventArgs e)
        {
            MaximizeRequested?.Invoke(sender, e);
        }

        private void Log(object sender, LogEventArgs e)
        {
            LogMessage?.Invoke(sender, e);
        }

        private void Log(LogEventArgs e)
        {
            Log(this, e);
        }

        private void Log(string message, ELogLevel level = ELogLevel.INFO)
        {
            Log(new LogEventArgs(message, level));
        }
        #endregion

        #region PROPERTIES
        private int _ID;
        [JsonProperty]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                if (_ID != value)
                {
                    _ID = value;
                    OnPropertyChanged("ID");
                }
            }
        }

        private Bitmap _Image;
        [JsonProperty]
        [JsonConverter(typeof(CustomBitmapConverter))]
        public Bitmap Image
        {
            get
            {
                return _Image;
            }
            set
            {
                if(value != _Image)
                {
                    _Image = value;
                    OnPropertyChanged("Image");
                    OnPropertyChanged("SearchAreaVisible");
                }
            }
        }

        private bool _AlwaysTrigger = false;
        [JsonProperty]
        public bool AlwaysTrigger
        {
            get
            {
                return _AlwaysTrigger;
            }
            set
            {
                if(value != _AlwaysTrigger)
                {
                    _AlwaysTrigger = value;
                    OnPropertyChanged("AlwaysTrigger");
                    RequestMarkDirty(new EventArgs());
                }
            }
        }

        private bool _Proceed = true;
        [JsonProperty]
        public bool Proceed
        {
            get
            {
                return _Proceed;
            }
            set
            {
                if (_Proceed != value)
                {
                    _Proceed = value;
                    OnPropertyChanged("Proceed");
                    RequestMarkDirty(new EventArgs());
                }
            }
        }

        private System.Drawing.Point _SearchAreaTopLeft;
        [JsonProperty]
        public System.Drawing.Point SearchAreaTopLeft
        {
            get
            {
                return _SearchAreaTopLeft;
            }
            set
            {
                if(value != _SearchAreaTopLeft)
                {
                    _SearchAreaTopLeft = value;
                    OnPropertyChanged("SearchAreaTopLeft");
                    OnPropertyChanged("SearchAreaVisible");
                }
            }
        }

        private System.Drawing.Size _SearchAreaSize;
        [JsonProperty]
        public System.Drawing.Size SearchAreaSize
        {
            get
            {
                return _SearchAreaSize;
            }
            set
            {
                if(value != _SearchAreaSize)
                {
                    _SearchAreaSize = value;
                    OnPropertyChanged("SearchAreaSize");
                    OnPropertyChanged("SearchAreaVisible");
                }
            }
        }

        private ObservableCollection<BaseAction> _Actions = new ObservableCollection<BaseAction>();
        [JsonProperty]
        public ObservableCollection<BaseAction> Actions
        {
            get
            {
                return _Actions;
            }
            set
            {
                if(_Actions != value)
                {
                    _Actions = value;
                    OnPropertyChanged("Actions");
                }
            }
        }

        private UserControl _Overlay;
        [JsonIgnore]
        public UserControl Overlay
        {
            get
            {
                return _Overlay;
            }
            set
            {
                if(_Overlay != value)
                {
                    _Overlay = value;
                    OnPropertyChanged("Overlay");
                }
            }
        }
        
        private string _ScreenshotButtonText = "Take Screenshot...";
        [JsonIgnore]
        public string ScreenshotButtonText
        {
            get
            {
                return _ScreenshotButtonText;
            }
            set
            {
                if (_ScreenshotButtonText != value)
                {
                    _ScreenshotButtonText = value;
                    OnPropertyChanged("ScreenshotButtonText");
                }
            }
        }

        [JsonIgnore]
        public bool SearchAreaVisible
        {
            get
            {
                return Image != null || SearchAreaTopLeft.X != 0 || SearchAreaTopLeft.Y != 0 || SearchAreaSize.Width != 0 | SearchAreaSize.Height != 0;
            }
        }
        #endregion

        #region VARIABLES
        [JsonIgnore]
        private AreaSelectionWindow areaSelectWindow;
        [JsonIgnore]
        private AreaSelectionViewModel viewModel;

        [JsonIgnore]
        private Timer timer = null;

        [JsonProperty]
        private List<int> ActionIDs = new List<int>();
        #endregion

        #region ICOMMANDS
        private ICommand _StartScreenshot;
        [JsonIgnore]
        public ICommand StartScreenshot
        {
            get
            {
                if (_StartScreenshot == null)
                {
                    _StartScreenshot = new RelayCommand(StartScreenshotEx, null);
                }
                return _StartScreenshot;
            }
        }
        private void StartScreenshotEx(object p)
        {
            if (ScreenshotButtonText == "Take Screenshot...")
            {
                RequestMinimize(new EventArgs());

                areaSelectWindow = new AreaSelectionWindow();

                viewModel = new AreaSelectionViewModel();

                viewModel.DataCaptured += FinishScreenshot;

                areaSelectWindow.DataContext = viewModel;

                areaSelectWindow.Show();
            }
            else
            {
                System.Media.SystemSounds.Exclamation.Play();
                Log("Cannot take a screenshot whilst a countdown is running to take a previous screenshot", ELogLevel.ERROR);
            }
        }

        private ICommand _CaptureArea;
        [JsonIgnore]
        public ICommand CaptureArea
        {
            get
            {
                if(_CaptureArea == null)
                {
                    _CaptureArea = new RelayCommand(CaptureAreaEx, null);
                }
                return _CaptureArea;
            }
        }
        private void CaptureAreaEx(object p)
        {
            if (ScreenshotButtonText == "Take Screenshot...")
            {
                RequestMinimize(new EventArgs());

                areaSelectWindow = new AreaSelectionWindow(true);

                viewModel = new AreaSelectionViewModel(true);

                viewModel.DataCaptured += FinishCaptureArea;

                areaSelectWindow.DataContext = viewModel;

                areaSelectWindow.Show();
            }
            else
            {
                System.Media.SystemSounds.Exclamation.Play();
                Log("Cannot capture area whilst a screenshot countdown is pending", ELogLevel.ERROR);
            }
        }

        private ICommand _CloseOverlay;
        [JsonIgnore]
        public ICommand CloseOverlay
        {
            get
            {
                if(_CloseOverlay == null)
                {
                    _CloseOverlay = new RelayCommand(CloseOverlayEx, null);
                }
                return _CloseOverlay;
            }
        }
        private void CloseOverlayEx(object p)
        {
            Overlay = null;
        }

        #region ADD ACTIONS
        private ICommand _AddAction;
        [JsonIgnore]
        public ICommand AddAction
        {
            get
            {
                if (_AddAction == null)
                {
                    _AddAction = new RelayCommand(AddActionEx, null);
                }
                return _AddAction;
            }
        }
        private void AddActionEx(object p)
        {
            Log("Opening add actions panel");
            Overlay = new ClickActionSelector(AddClickRelativeAction, AddClickAbsoluteAction, AddClickAbsoluteAreaAction, AddMoveRelativeAction, AddMoveAbsoluteAction, AddActivateWindowAction, AddPauseAction, AddSendStringAction, AddAbortAction, CloseOverlay);
        }

        private ICommand _AddClickRelativeAction;
        [JsonIgnore]
        public ICommand AddClickRelativeAction
        {
            get
            {
                if (_AddClickRelativeAction == null)
                {
                    _AddClickRelativeAction = new RelayCommand(AddClickRelativeActionEx, null);
                }
                return _AddClickRelativeAction;
            }
        }
        private void AddClickRelativeActionEx(object p)
        {
            Log("Adding click relative action");
            int nextActionID = GetNextActionID();
            ClickRelativeAction cra = new ClickRelativeAction(nextActionID);
            cra.MarkDirty += RequestMarkDirty;
            cra.MinimizeRequested += RequestMinimize;
            cra.MaximizeRequested += RequestMaximize;
            cra.LogMessage += Log;
            Actions.Add(cra);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddClickAbsoluteAction;
        [JsonIgnore]
        public ICommand AddClickAbsoluteAction
        {
            get
            {
                if (_AddClickAbsoluteAction == null)
                {
                    _AddClickAbsoluteAction = new RelayCommand(AddClickAbsoluteActionEx, null);
                }
                return _AddClickAbsoluteAction;
            }
        }
        private void AddClickAbsoluteActionEx(object p)
        {
            Log("Adding click absolute action");
            int nextActionID = GetNextActionID();
            ClickAbsoluteAction caa = new ClickAbsoluteAction(nextActionID);
            caa.MarkDirty += RequestMarkDirty;
            caa.MinimizeRequested += RequestMinimize;
            caa.MaximizeRequested += RequestMaximize;
            caa.LogMessage += Log;
            Actions.Add(caa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddClickAbsoluteAreaAction;
        [JsonIgnore]
        public ICommand AddClickAbsoluteAreaAction
        {
            get
            {
                if (_AddClickAbsoluteAreaAction == null)
                {
                    _AddClickAbsoluteAreaAction = new RelayCommand(AddClickAbsoluteAreaActionEx, null);
                }
                return _AddClickAbsoluteAreaAction;
            }
        }
        private void AddClickAbsoluteAreaActionEx(object p)
        {
            Log("Adding click absolute area action");
            int nextActionID = GetNextActionID();
            ClickAbsoluteAreaAction caaa = new ClickAbsoluteAreaAction(nextActionID);
            caaa.MarkDirty += RequestMarkDirty;
            caaa.MinimizeRequested += RequestMinimize;
            caaa.MaximizeRequested += RequestMaximize;
            caaa.LogMessage += Log;
            Actions.Add(caaa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddMoveRelativeAction;
        [JsonIgnore]
        public ICommand AddMoveRelativeAction
        {
            get
            {
                if (_AddMoveRelativeAction == null)
                {
                    _AddMoveRelativeAction = new RelayCommand(AddMoveRelativeActionEx, null);
                }
                return _AddMoveRelativeAction;
            }
        }
        private void AddMoveRelativeActionEx(object p)
        {
            Log("Adding move relative action");
            int nextActionID = GetNextActionID();
            MoveRelativeAction mra = new MoveRelativeAction(nextActionID);
            mra.MarkDirty += RequestMarkDirty;
            mra.MinimizeRequested += RequestMinimize;
            mra.MaximizeRequested += RequestMaximize;
            mra.LogMessage += Log;
            Actions.Add(mra);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddMoveAbsoluteAction;
        [JsonIgnore]
        public ICommand AddMoveAbsoluteAction
        {
            get
            {
                if (_AddMoveAbsoluteAction == null)
                {
                    _AddMoveAbsoluteAction = new RelayCommand(AddMoveAbsoluteActionEx, null);
                }
                return _AddMoveAbsoluteAction;
            }
        }
        private void AddMoveAbsoluteActionEx(object p)
        {
            Log("Adding move absolute action");
            int nextActionID = GetNextActionID();
            MoveAbsoluteAction maa = new MoveAbsoluteAction(nextActionID);
            maa.MarkDirty += RequestMarkDirty;
            maa.MinimizeRequested += RequestMinimize;
            maa.MaximizeRequested += RequestMaximize;
            maa.LogMessage += Log;
            Actions.Add(maa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddActivateWindowAction;
        [JsonIgnore]
        public ICommand AddActivateWindowAction
        {
            get
            {
                if (_AddActivateWindowAction == null)
                {
                    _AddActivateWindowAction = new RelayCommand(AddActivateWindowActionEx, null);
                }
                return _AddActivateWindowAction;
            }
        }
        private void AddActivateWindowActionEx(object p)
        {
            Log("Adding activate window action");
            int nextActionID = GetNextActionID();
            ActivateWindowAction awa = new ActivateWindowAction(nextActionID);
            awa.MarkDirty += RequestMarkDirty;
            awa.MinimizeRequested += RequestMinimize;
            awa.MaximizeRequested += RequestMaximize;
            awa.LogMessage += Log;
            Actions.Add(awa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddPauseAction;
        [JsonIgnore]
        public ICommand AddPauseAction
        {
            get
            {
                if (_AddPauseAction == null)
                {
                    _AddPauseAction = new RelayCommand(AddPauseActionEx, null);
                }
                return _AddPauseAction;
            }
        }
        private void AddPauseActionEx(object p)
        {
            Log("Adding pause action");
            int nextActionID = GetNextActionID();
            PauseAction pa = new PauseAction(nextActionID);
            pa.MarkDirty += RequestMarkDirty;
            pa.MinimizeRequested += RequestMinimize;
            pa.MaximizeRequested += RequestMaximize;
            pa.LogMessage += Log;
            Actions.Add(pa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddSendStringAction;
        [JsonIgnore]
        public ICommand AddSendStringAction
        {
            get
            {
                if (_AddSendStringAction == null)
                {
                    _AddSendStringAction = new RelayCommand(AddSendStringActionEx, null);
                }
                return _AddSendStringAction;
            }
        }
        private void AddSendStringActionEx(object p)
        {
            Log("Adding send string action");
            int nextActionID = GetNextActionID();
            SendStringAction ssa = new SendStringAction(nextActionID);
            ssa.MarkDirty += RequestMarkDirty;
            ssa.MinimizeRequested += RequestMinimize;
            ssa.MaximizeRequested += RequestMaximize;
            ssa.LogMessage += Log;
            Actions.Add(ssa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _AddAbortAction;
        [JsonIgnore]
        public ICommand AddAbortAction
        {
            get
            {
                if (_AddAbortAction == null)
                {
                    _AddAbortAction = new RelayCommand(AddAbortActionEx, null);
                }
                return _AddAbortAction;
            }
        }
        private void AddAbortActionEx(object p)
        {
            Log("Adding abort action");
            int nextActionID = GetNextActionID();
            AbortAction aa = new AbortAction(nextActionID);
            aa.LogMessage += Log;
            aa.MarkDirty += RequestMarkDirty;
            aa.MinimizeRequested += RequestMinimize;
            aa.MaximizeRequested += RequestMaximize;
            Actions.Add(aa);
            ActionIDs.Add(nextActionID);
            CloseOverlay.Execute(null);
            RequestMarkDirty(new EventArgs());
        }
        #endregion

        #region MANAGE ACTIONS
        private ICommand _RemoveAction;
        [JsonIgnore]
        public ICommand RemoveAction
        {
            get
            {
                if (_RemoveAction == null)
                {
                    _RemoveAction = new RelayCommand(RemoveActionEx, null);
                }
                return _RemoveAction;
            }
        }
        private void RemoveActionEx(object p)
        {
            int id = (int)p;
            Log("Removing action ID: " + id);
            BaseAction ba = Actions.Single(a => a.ID == id);
            ActionIDs.Remove(ba.ID);
            Actions.Remove(ba);
            RequestMarkDirty(new EventArgs());
        }

        private ICommand _MoveActionUp;
        [JsonIgnore]
        public ICommand MoveActionUp
        {
            get
            {
                if (_MoveActionUp == null)
                {
                    _MoveActionUp = new RelayCommand(MoveActionUpEx, null);
                }
                return _MoveActionUp;
            }
        }
        private void MoveActionUpEx(object p)
        {
            int id = (int)p;
            BaseAction ba = Actions.Single(a => a.ID == id);
            int index = Actions.IndexOf(ba);
            if(index > 0)
            {
                Log("Moving action ID: " + id + " upwards");
                Actions.Move(index, index - 1);
                RequestMarkDirty(new EventArgs());
            }
        }

        private ICommand _MoveActionDown;
        [JsonIgnore]
        public ICommand MoveActionDown
        {
            get
            {
                if (_MoveActionDown == null)
                {
                    _MoveActionDown = new RelayCommand(MoveActionDownEx, null);
                }
                return _MoveActionDown;
            }
        }
        private void MoveActionDownEx(object p)
        {
            int id = (int)p;
            BaseAction ba = Actions.Single(a => a.ID == id);
            int index = Actions.IndexOf(ba);
            if(index < Actions.Count - 1)
            {
                Log("Moving action ID: " + id + " downwards");
                Actions.Move(index, index + 1);
                RequestMarkDirty(new EventArgs());
            }
        }
        #endregion
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }

        #endregion
    }
}
