﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class ActivateWindowAction : BaseAction
    {
        public ActivateWindowAction() : base()
        {

        }

        public ActivateWindowAction(int ID) : base(ID)
        {
            
        }

        public override void Run(Rectangle location)
        {
            Log("Activating window with title: " + Title, ELogLevel.ACTION);
            WinAPI.ActivateWindow(Title);
        }

        #region PROPERTIES
        private string _Title;
        public string Title
        {
            get
            {
                return _Title;
            }
            set
            {
                _Title = value;
                OnPropertyChanged("Title");
            }
        }
        #endregion
    }
}
