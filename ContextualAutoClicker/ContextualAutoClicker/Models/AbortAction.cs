﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class AbortAction : BaseAction
    {
        public AbortAction() : base()
        {

        }

        public AbortAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle location)
        {
            Log("Abort action encountered - Halt everything", ELogLevel.ACTION);
            throw new NotImplementedException();
        }
    }
}
