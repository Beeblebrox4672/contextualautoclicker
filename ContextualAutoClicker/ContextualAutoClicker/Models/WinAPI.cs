﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace ContextualAutoClicker.Models
{
    class WinAPI
    {
        [DllImport("user32.dll")]
        private static extern bool GetCursorPos(out System.Drawing.Point lpPoint);

        public static System.Drawing.Point GetCursorPosition()
        {
            System.Drawing.Point lpPoint;
            GetCursorPos(out lpPoint);

            return lpPoint;
        }

        public static Bitmap GetFullScreenShot()
        {
            var bmpScreenshot = new Bitmap((int)SystemParameters.VirtualScreenWidth, (int)SystemParameters.VirtualScreenHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // Create a graphics object from the bitmap.
            var gfxScreenshot = Graphics.FromImage(bmpScreenshot);

            // Take the screenshot from the upper left corner to the right bottom corner.
            gfxScreenshot.CopyFromScreen(0, 0, 0, 0, new System.Drawing.Size((int)SystemParameters.VirtualScreenWidth, (int)SystemParameters.VirtualScreenHeight), CopyPixelOperation.SourceCopy);

            return bmpScreenshot;
        }

        public static Bitmap CropImage(Bitmap source, System.Drawing.Point topLeft, System.Drawing.Size size)
        {
            Rectangle cropRect = new Rectangle(topLeft.X, topLeft.Y, size.Width, size.Height);

            Bitmap croppedImage = new Bitmap(cropRect.Width, cropRect.Height);

            using (Graphics g = Graphics.FromImage(croppedImage))
            {
                g.DrawImage(source, new Rectangle(0, 0, croppedImage.Width, croppedImage.Height), cropRect, GraphicsUnit.Pixel);
            }

            return croppedImage;
        }

        public static Rectangle SearchBitmapLocation(Bitmap smallBmp, Bitmap bigBmp, double tolerance)
        {
            BitmapData SearchBitmapsmallData;
            BitmapData SearchBitmapbigData;
            Rectangle SearchBitmapLocation;
            int SearchBitmapSmallStride;
            int SearchBitmapBigStride;
            int SearchBitmapBigWidth;
            int SearchBitmapBigHeight;
            int SearchBitmapSmallWidth;
            int SearchBitmapSmallHeight;
            int SearchBitmapMargin;

            SearchBitmapsmallData =
              smallBmp.LockBits(new Rectangle(0, 0, smallBmp.Width, smallBmp.Height),
                       System.Drawing.Imaging.ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format24bppRgb);
            SearchBitmapbigData =
              bigBmp.LockBits(new Rectangle(0, 0, bigBmp.Width, bigBmp.Height),
                       System.Drawing.Imaging.ImageLockMode.ReadOnly,
                       System.Drawing.Imaging.PixelFormat.Format24bppRgb);

            SearchBitmapSmallStride = SearchBitmapsmallData.Stride;
            SearchBitmapBigStride = SearchBitmapbigData.Stride;

            SearchBitmapBigWidth = bigBmp.Width;
            SearchBitmapBigHeight = bigBmp.Height - smallBmp.Height + 1;
            SearchBitmapSmallWidth = smallBmp.Width * 3;
            SearchBitmapSmallHeight = smallBmp.Height;

            SearchBitmapLocation = Rectangle.Empty;
            SearchBitmapMargin = Convert.ToInt32(255.0 * tolerance);

            unsafe
            {
                byte* pSmall = (byte*)(void*)SearchBitmapsmallData.Scan0;
                byte* pBig = (byte*)(void*)SearchBitmapbigData.Scan0;

                int smallOffset = SearchBitmapSmallStride - smallBmp.Width * 3;
                int bigOffset = SearchBitmapBigStride - bigBmp.Width * 3;

                bool matchFound = true;

                for (int y = 0; y < SearchBitmapBigHeight; y++)
                {
                    for (int x = 0; x < SearchBitmapBigWidth; x++)
                    {
                        byte* pBigBackup = pBig;
                        byte* pSmallBackup = pSmall;

                        //Look for the small picture.
                        for (int i = 0; i < SearchBitmapSmallHeight; i++)
                        {
                            int j = 0;
                            matchFound = true;
                            for (j = 0; j < SearchBitmapSmallWidth; j++)
                            {
                                //With tolerance: pSmall value should be between margins.
                                int inf = pBig[0] - SearchBitmapMargin;
                                int sup = pBig[0] + SearchBitmapMargin;
                                if (sup < pSmall[0] || inf > pSmall[0])
                                {
                                    matchFound = false;
                                    break;
                                }

                                pBig++;
                                pSmall++;
                            }

                            if (!matchFound) break;

                            //We restore the pointers.
                            pSmall = pSmallBackup;
                            pBig = pBigBackup;

                            //Next rows of the small and big pictures.
                            pSmall += SearchBitmapSmallStride * (1 + i);
                            pBig += SearchBitmapBigStride * (1 + i);
                        }

                        //If match found, we return.
                        if (matchFound)
                        {
                            SearchBitmapLocation.X = x;
                            SearchBitmapLocation.Y = y;
                            SearchBitmapLocation.Width = smallBmp.Width;
                            SearchBitmapLocation.Height = smallBmp.Height;
                            break;
                        }
                        //If no match found, we restore the pointers and continue.
                        else
                        {
                            pBig = pBigBackup;
                            pSmall = pSmallBackup;
                            pBig += 3;
                        }
                    }

                    if (matchFound) break;

                    pBig += bigOffset;
                }
            }

            bigBmp.UnlockBits(SearchBitmapbigData);
            smallBmp.UnlockBits(SearchBitmapsmallData);

            return SearchBitmapLocation;
        }

        public static bool SearchBitmap(Bitmap smallBmp, Bitmap bigBmp, double tolerance)
        {
            Rectangle location = SearchBitmapLocation(smallBmp, bigBmp, tolerance);
            return location.Width > 0 || location.Height > 0;
        }

        #region MOUSE
        [DllImport("User32.Dll", SetLastError = true)]
        public static extern bool SetCursorPos(int x, int y);

        [DllImport("user32.dll")]
        public static extern void mouse_event(int dwFlags, int dx, int dy, int dwData, int dwExtraInfo);

        public const int MOUSE_LeftDown = 0x00000002;
        public const int MOUSE_LeftUp = 0x00000004;
        public const int MOUSE_MiddleDown = 0x00000020;
        public const int MOUSE_MiddleUp = 0x00000040;
        public const int MOUSE_Move = 0x00000001;
        public const int MOUSE_Absolute = 0x00008000;
        public const int MOUSE_RightDown = 0x00000008;
        public const int MOUSE_RightUp = 0x00000010;

        public static void MoveMouse(System.Drawing.Point location)
        {
            SetCursorPos(location.X, location.Y);
        }

        public static void ClickMouse(System.Drawing.Point location)
        {
            mouse_event(MOUSE_LeftDown | MOUSE_LeftUp, location.X, location.Y, 0, 0);
        }

        public static void MoveAndClick(System.Drawing.Point location)
        {
            MoveMouse(location);

            Thread.Sleep(15);

            ClickMouse(location);
        }
        #endregion

        #region WINDOW
        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        static extern bool SetForegroundWindow(IntPtr hWnd);

        public static void ActivateWindow(string windowTitle)
        {
            IntPtr hWnd = FindWindow(null, windowTitle);
            if(hWnd != IntPtr.Zero)
                SetForegroundWindow(hWnd);
        }
        #endregion

        public const int WM_KEYDOWN = 0x100;
        public const int WM_KEYUP = 0x101;

        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        public static extern bool PostMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

        [DllImport("user32.dll")]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vk);

        [DllImport("user32.dll")]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        //Modifiers:
        public const uint MOD_NONE = 0x0000; //[NONE]
        public const uint MOD_ALT = 0x0001; //ALT
        public const uint MOD_CONTROL = 0x0002; //CTRL
        public const uint MOD_SHIFT = 0x0004; //SHIFT
        public const uint MOD_WIN = 0x0008; //WINDOWS
                                            //CAPS LOCK:
        public const uint VK_CAPITAL = 0x14;
    }
}
