﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContextualAutoClicker.Models
{
    public class PauseAction : BaseAction
    {
        public PauseAction() : base()
        {

        }

        public PauseAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle location)
        {
            Log("Pausing for: " + PauseTime + "ms", ELogLevel.ACTION);
            Thread.Sleep(PauseTime);
        }

        #region PROPERTIES
        private int _PauseTime = 1000;
        public int PauseTime
        {
            get
            {
                return _PauseTime;
            }
            set
            {
                if(_PauseTime != value)
                {
                    _PauseTime = value;
                    OnPropertyChanged("PauseTime");
                    RequestMarkDirty(new EventArgs());
                }
            }
        }
        #endregion
    }
}
