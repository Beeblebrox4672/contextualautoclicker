﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class MaxAttemptsException : Exception
    {
        public MaxAttemptsException()
        {
        }

        public MaxAttemptsException(string message)
            : base(message)
        {
        }

        public MaxAttemptsException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
