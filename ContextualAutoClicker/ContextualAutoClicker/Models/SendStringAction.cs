﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsInput;

namespace ContextualAutoClicker.Models
{
    class SendStringAction : BaseAction
    {
        public SendStringAction() : base()
        {

        }

        public SendStringAction(int ID) : base(ID)
        {

        }

        public override void Run(Rectangle location)
        {
            Log("Sending string: " + Keys, ELogLevel.ACTION);
            inputSimulator.Keyboard.TextEntry(Keys);
        }

        #region VARIABLES
        InputSimulator inputSimulator = new InputSimulator();
        #endregion

        #region PROPERTIES
        private string _Keys;
        public string Keys
        {
            get
            {
                return _Keys;
            }
            set
            {
                _Keys = value;
                OnPropertyChanged("Keys");
                RequestMarkDirty(new EventArgs());
            }
        }
        #endregion
    }
}
