﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class AbortEncounteredException : Exception
    {
        public AbortEncounteredException()
        {
        }

        public AbortEncounteredException(string message)
            : base(message)
        {
        }

        public AbortEncounteredException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
