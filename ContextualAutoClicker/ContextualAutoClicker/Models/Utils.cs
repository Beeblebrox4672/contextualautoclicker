﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ContextualAutoClicker.Models
{
    class Utils
    {
        public Utils()
        {

        }

        public static string GetDescription(Enum en)
        {
            Type type = en.GetType();
            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        public static Dictionary<T, string> GetEnumDictionary<T>()
        {
            return Enum.GetValues(typeof(T))
                        .Cast<T>()
                        .ToDictionary(VT => VT, VT => GetDescription(VT as Enum));
        }
    }
}
