﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace ContextualAutoClicker.Models
{
    /// <summary>
    /// Actions consist of:
    /// Click Relative
    /// Click Absolute
    /// Move Relative
    /// Move Absolute
    /// Activate Window
    /// Pause
    /// Send string
    /// Abort
    /// </summary>
    public abstract class BaseAction : INotifyPropertyChanged
    {
        public BaseAction()
        {

        }

        public BaseAction(int ID)
        {
            this.ID = ID;
        }

        public abstract void Run(Rectangle location);

        private int _ID;
        [JsonProperty]
        public int ID
        {
            get
            {
                return _ID;
            }
            set
            {
                _ID = value;
                OnPropertyChanged("ID");
            }
        }

        #region EVENTS
        public event EventHandler MinimizeRequested;
        public event EventHandler MaximizeRequested;
        public event EventHandler MarkDirty;
        public event EventHandler<LogEventArgs> LogMessage;

        protected virtual void RequestMarkDirty(EventArgs e)
        {
            MarkDirty?.Invoke(this, e);
        }

        protected virtual void RequestMinimize(EventArgs e)
        {
            MinimizeRequested?.Invoke(this, e);
        }

        protected virtual void RequestMaximize(EventArgs e)
        {
            MaximizeRequested?.Invoke(this, e);
        }

        protected void Log(object sender, LogEventArgs e)
        {
            LogMessage?.Invoke(sender, e);
        }

        protected void Log(LogEventArgs e)
        {
            Log(this, e);
        }

        protected void Log(string message, ELogLevel level = ELogLevel.INFO)
        {
            Log(new LogEventArgs(message, level));
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;

            if (handler != null)
            {
                if (handler.Target is CollectionView)
                {
                    ((CollectionView)handler.Target).Refresh();
                }
                else
                {
                    handler(this, new PropertyChangedEventArgs(propertyName));
                }
            }
        }
        #endregion
    }
}
